import { Component, OnInit, DoCheck, NgZone } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  counter: number = 0;
  
  constructor(private _ngZone: NgZone) {}
  
  ngOnInit() {
    this._ngZone.runOutsideAngular(() => {
      for (let i = 0, iMax = 10; i < iMax; ++i) {
        setTimeout(() => ++this.counter);
      }  
      this._ngZone.run(() => {
        setTimeout(() => this.counter = this.counter, 1000);
      });
    });

  }
  
  ngDoCheck() {
    console.log('Change detection has been run!');
  }
}
